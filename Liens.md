# Liste de liens pointant vers des publications et autres supports utiles pour le cours

* Article de James McKeen Cattell, ["Homo Scientificus Americanus"](https://brocku.ca/MeadProject/Cattell/Cattell_1903a.html), 1903
* Article de Robert K. Merton, ["The Matthew Effect in Science"](http://www.garfield.library.upenn.edu/merton/matthew1.pdf), 1968
* Recension par Xavier Polanco de l'ouvrage d'Alphonse de Candolle, ["Alphonse de Candolle, Histoire des sciences et des savants depuis deux siècles, d'après l'opinion des principales académies ou sociétés scientifiques. Texte revu par Bruno Latour (Paris : Fayard, 1987)"](https://www.persee.fr/doc/rhs_0151-4105_1990_num_43_2_4171_t1_0365_0000_1), 1990
* Article de Margaret W. Rossiter, ["The Matthew Matilda Effect in Science "](https://journals-sagepub-com.inshs.bib.cnrs.fr/doi/10.1177/030631293023002004), 1993
* Essai de Xavier Polanco, ["Aux sources de la bibliométrie"](http://gabriel.gallezot.free.fr/Solaris/d02/2polanco1.html), 1995
* Article de Pierre Verdrager, ["La sociologie de la reconnaissance scientifique : généalogie et perspectives"](https://www.cairn.info/revue-histoire-des-sciences-humaines-2005-2-page-51.htm), 2005
* Article d'Yves Gingras, ["Du mauvais usage de faux indicateurs"](https://www.cairn.info/revue-d-histoire-moderne-et-contemporaine-2008-5-page-67.htm), 2008 
* Rapport d'information du Sénat n° 442 (2007-2008) de M. Joël BOURDIN, ["Enseignement supérieur : le défi des classements"](https://www.senat.fr/rap/r07-442/r07-442.html), 2008
* Article d'Hervé Théry, ["Palmarès des universités mondiales, « Shanghai » et les autres"](https://mappemonde-archive.mgm.fr/num24/articles/art09401.pdf), 2009
* Recension par Claire Lemercier de [Michèle LAMONT, "How Professors Think. Inside the Curious World of Academic Judgment". Recension](https://laviedesidees.fr/IMG/pdf/20090528_lemercier.pdf), 2009
* Article de Manuel Durand-Barthez, ["Entre Lolf et Shanghai"](http://bbf.enssib.fr/consulter/bbf-2010-04-0006-001), 2010
* Rapport de la Commission Européenne, ["Assessing Europe’s University-Based Research"](http://ec.europa.eu/research/science-society/document_library/pdf_06/assessing-europe-university-based-research_en.pdf), 2010
* Article de Sari Hanafi, ["University systems in the Arab East: Publish globally and perish locally vs publish locally and perish globally"](https://staff.aub.edu.lb/~sh41/dr_sarry_website/publications/2011_Publish%20globally_current.pdf), 2011
* Article de David Pontille et Didier Torny, ["La manufacture de l'évaluation scientifique"](https://www.cairn.info/revue-reseaux-2013-1-page-23.htm), 2013
* Article de Vincente Safon, ["What do global university rankings really measure? The search for the X factor and the X entity"](http://link.springer.com/10.1007/s11192-013-0986-8), 2013
* Entretien entre Simon Paye, (_la Vie des Idées_) et Yves Gingras ["La fièvre de l'évaluation"](https://laviedesidees.fr/IMG/pdf/20140916_gingras.pdf), 2014
* Chronique d'Yves Gingras, ["How to boost your university up the rankings"](http://www.universityworldnews.com/article.php?story=20140715142345754), 2014
* Article de Cong Cao, ["The universal values of science and China's Nobel Prize pursuit"](http://eprints.nottingham.ac.uk/29036/1/Nobel%20Prize%20and%20the%20Universal%20Value%20of%20Science.pdf), 2014
* Thèse de Marion Maisonobe, ["Étudier la géographie des activités et des collectifs scientifiques dans le monde : de la croissance du système de production contemporain aux dynamiques d'une spécialité, la réparation de l'ADN"](https://tel.archives-ouvertes.fr/tel-01235015), 2015
* Actes de l’édition 2015 du séminaire Ilya Prigogine _Penser la Science_, ["L’Évaluation de la recherche en Question(s)"](http://penserlascience.ulb.ac.be/IMG/pdf/penserlascience_2015_actes_complets.pdf), 2015
* Manifeste de Diana Hicks, Paul Wouters, Ludo Waltman, Sarah de Rijcke et Ismael Rafols, ["Bibliometrics: The Leiden Manifesto for research metrics"](https://www.nature.com/news/bibliometrics-the-leiden-manifesto-for-research-metrics-1.17351), 2015
* Chronique de Vincent Larivière, Stefanie Haustein et Philippe Mongeon, ["L’oligopole des grands éditeurs savants"](https://www.acfas.ca/publications/decouvrir/2015/02/l-oligopole-grands-editeurs-savants), 2015
* Note de Campus France, ["L’impact des classements internationaux  des établissements d’enseignement supérieur en France et dans le monde"](https://www.campusfrance.org/fr/ressource/l-impact-des-classements-internationaux-des-etablissements-d-enseignement-superieur-en), 2015
* Emission Affaire Etrangères sur _France culture_, ["La compétition mondiale  des universités"](https://www.franceculture.fr/emissions/affaires-etrangeres/la-competition-mondiale-des-universites), 2016
* Article de Henk Moed, ["A critical comparative analysis of five world university rankings"](https://arxiv.org/ftp/arxiv/papers/1611/1611.06547.pdf), 2016
* Article de Dag W. Aksnes, Gunnar Sivertsen, Thed N. Van Leeuwen et Kaja K. Wendt, ["Measuring the productivity of national R&D systems: Challenges in cross-national comparisons of R&D input and publication output indicators"](https://academic.oup.com/spp/article/44/2/246/2525560), 2016
* Article de Björn Hammarfelt, Sarah de Rijcke, et Paul Wouters, ["From Eminent Men to Excellent Universities: University Rankings as Calculative Devices"](https://openaccess.leidenuniv.nl/bitstream/handle/1887/57650/From_eminent_men_to_excellent_universities.pdf?sequence=1), 2017
* Carte de Behzad Ataie-Ashtiani, ["World map of scientific misconduct"](https://link.springer.com/article/10.1007%2Fs11948-017-9939-6#citeas), 2018
* Article d'Yves Gingras, ["Les transformations de la production du savoir : de l’unité de connaissance à l’unité comptable"](https://www.cairn.info/revue-zilsel-2018-2-p-139.htm), 2018
* Appel de Jean Frances, Yves Gingras, Philippe Huneman et Arnaud Saint-Martin paru dans _Le Monde_ du 30 octobre, ["Canulars scientifiques, revues prédatrices et « slow science »"](https://abonnes.lemonde.fr/sciences/article/2018/10/30/canulars-scientifiques-revues-predatrices-et-slow-science_5376660_1650684.html?"), 2018
* Article de Marion Maisonobe, Laurent Jégou et Guillaume Cabanac, ["Peripheral forces"](https://www.nature.com/articles/d41586-018-07210-6), 2018
* Article de Jacek Pietrucha, ["Country-specific determinants of world university rankings"](https://ideas.repec.org/a/spr/scient/v114y2018i3d10.1007_s11192-017-2634-1.html), 2018
* Blog de Richard Holmes centré sur la critique des résultats de classements, ["ranking watch"](http://rankingwatch.blogspot.com/), dernier post daté de septembre 2019
* Un rapport de l'EUA (European University Association), ["University Mergers in Europe"](https://eua.eu/downloads/publications/eua%20merger%20brief%202904.pdf), 2019
* Edito de Marion Maisonobe, Laurent Jégou et Guillaume Cabanac, ["L'excellence partout : la percée des villes périphériques à l'échelle mondiale"](https://www6.inra.fr/caps-publierlascience/Les-editos/Edito-PLAS-23/L-excellence-partout-la-percee-des-villes-peripheriques-a-l-echelle-mondiale), 2019
* Tribune de Paul Wouters, Cassidy R. Sugimoto, Vincent Larivière, Marie E. McVeigh, Bernd Pulverer, Sarah de Rijcke et Ludo Waltman, ["Rethinking impact factors: better ways to judge a journal"](https://www.nature.com/articles/d41586-019-01643-3), 2019
* Editorial de Dalmeet Singh Chawla, ["Study quantifies the growing traction of open access"](https://physicstoday.scitation.org/do/10.1063/PT.6.2.20190418a/full/), 2019
* Brève de Richard Van Noorden et Dalmeet Singh Chawla,  ["Hundreds of extreme self-citing scientists revealed in new database"](https://www.nature.com/articles/d41586-019-02479-7), 2019
* Communiqué du MESRI, [Publication du Classement de Shanghai 2019 : réaction de Frédérique Vidal](https://www.enseignementsup-recherche.gouv.fr/cid144243/publication-du-classement-de-shanghai-2019-reaction-de-frederique-vidal.html), 2019
* Chronique de Ellie Bothwell dans le THE magazine, ["Award Nobels to teams, not individual ‘heroes’, say scientists"](https://www.timeshighereducation.com/news/award-nobels-teams-not-individual-heroes-say-scientists), octobre 2019
* Article de Pierre Courtioux, François Métivier et Antoine Reberioux, ["Scientific Competition between Countries: Did China Get What It Paid for?"](https://halshs.archives-ouvertes.fr/halshs-02307534/document), octobre 2019
* Chronique de Vincent Larivière et Cassidy R. Sugimoto, ["Des classements pour le bien commun"](https://www.acfas.ca/publications/decouvrir/2019/10/classements-bien-commun), octobre 2019
* Article d'Alberto Baccini, Giuseppe De Nicolao, et Eugenio Petrovich, ["Citation gaming induced by bibliometric evaluation: A country-level comparative analysis"](https://doi.org/10.1371/journal.pone.0221212), 2019
* Article de Nicolas Robinson-Garcia, Rodrigo Costas et Thed N. van Leeuwen, ["Indicators of Open Access for universities"](https://arxiv.org/abs/1906.03840), octobre 2019
* Brève d'US News suite à la sortie de son classement universitaire, ["Where in the World Are the Best Global Universities?"](https://www.usnews.com/education/best-global-universities/articles/map-where-in-the-world-are-the-best-global-universities), octobre 2019
* Billet de Jake Goldenfein, Sebastian Benthall, Daniel Griffin et Eran Toch, ["Private Companies and Scholarly Infrastructure – Google Scholar and Academic Autonomy"](https://www.dli.tech.cornell.edu/post/private-companies-and-scholarly-infrastructure-google-scholar-and-academic-autonomy), octobre 2019
* Site internet de l'IREG, [IREG Observatory on Academic Ranking and Excellence](http://ireg-observatory.org/en/)
* Guide interactif de la bibliothèque de Montréal par Vincent Larivière, ["Les mesures d'impact de la production scientifique"](http://guides.bib.umontreal.ca/disciplines/38-Mesures-d-impact-de-la-production-scientifique?tab=2761)
* Article de Lin Zhang et Gunnar Sivertsen, ["The New Research Assessment Reform in China and Its Implementation"](https://www.scholarlyassessmentreports.org/articles/10.29024/sar.15/), 2020
* Note d'actualité d'Ewen Callaway, ["Will the pandemic permanently alter scientific publishing?"](https://www.nature.com/articles/d41586-020-01520-4), 2020
* Note de Sophie Baudet-Michel, Sandrine Berroir, Claude Grasland, Marianne Guérois, Malika Madelin, Marion Maisonobe, Pierre Pistre, Josyane Ronchail, Christine Zanin, ["Vers une désertification scientifique et universitaire du territoire français ?"](https://halshs.archives-ouvertes.fr/halshs-02943730/document), 2020
* Article de conférence de Michel Grossetti, Marion Maisonobe, Laurent Jégou, Béatrice Milard et Guillaume Cabanac, ["L’organisation spatiale de la recherche française à travers les publications savantes : régularité des tendances de long terme et désordre des politiques publiques (1999-2017)"](https://hal.archives-ouvertes.fr/hal-02627291/document), 2020
* Note d'Yves Gingras et Medhi Khelfaoui, ["L'effet SIGAPS : la recherche médicale française sous l'emprise de l'évaluation comptable"](https://cirst2.openum.ca/files/sites/179/2020/10/Note_2020-05vf.pdf), 2020
* Entretien avec Michel Grossetti, ["Compétitivité et attractivité des métropoles, des mythes territoriaux"](https://www.revue-urbanites.fr/14-grossetti/), 2020
* Article de Pierre Papon, ["Les métropoles mondiales de recherche : un duo sino-américain ?"](https://www.futuribles.com/fr/article/les-metropoles-mondiales-de-recherche-un-duo-sino-/), 2020
* Prépublication de Guillaume Cabanac, Alexandre Clausse, Laurent Jégou et Marion Maisonobe, [The Geography of Retracted Papers: Showcasing a Crossref–Dimensions–NETSCITY Pipeline for the Spatial Analysis of Bibliographic Data](https://dapp.orvium.io/deposits/6442fee5c93d17c257de17d2/view), 2023
* Commmuniqué de presse de l'Université d'Utrecht, [Why UU is missing in the THE ranking](https://www.uu.nl/en/news/why-uu-is-missing-in-the-the-ranking), septembre 2023
* Article d'Anika Surmeier, Alex Bignotti, Bob Doherty, David Littlewood, Diane Holt, Phyllis Awor, Ralph Hamann et Teddy Ossei Kwakye, paru dans _The Conversation_ le 31 octobre, [Les classements mondiaux des universités tiennent désormais compte de l'impact social : les universités africaines démarrent en force](https://theconversation.com/les-classements-mondiaux-des-universites-tiennent-desormais-compte-de-limpact-social-les-universites-africaines-demarrent-en-force-216661), 2023
* Article de Leigh-Ann Butler, Lisa Matthias, Marc-André Simard, Philippe Mongeon et Stéphanie Haussein, [The Oligopoly’s Shift to Open Access. How the Big Five Academic Publishers Profit from Article Processing Charges](https://direct.mit.edu/qss/article/doi/10.1162/qss_a_00272/118070/The-Oligopoly-s-Shift-to-Open-Access-How-the-Big), 2023

# Ouvrages
* Ouvrage numérisé d'Alphonse de Candolle, ["Histoire des sciences et des savants depuis deux siècles"](https://archive.org/details/histoiredesscie00candgoog/page/n7), 1873
* Little science, big science, Derek J. de Solla Price, 1963, Columbia University Press, [réédition de 1986 en ligne](http://derekdesollaprice.org/little-science-big-science-full-text/)
* Science, argent et politique. Un essai d'interprétation. Dominique Pestre, 2003, Editions Quæ, [en ligne](https://www.cairn.info/science-argent-et-politique--9782738011008.htm)
* Les dérives de l’évaluation de la recherche. Du bon usage de la bibliométrie, Yves Gingras, 2014, Editions RAISONS D’AGIR
* Benchmarking: L’État sous pression statistique, Isabelle Bruno et Emmanuel Didier, 2015, La Découverte, coll.« ZONES »
* Les ancrages nationaux de la science mondiale. XVIIIe-XXIe siècles, dirigé par Mina Kleiche, 2018, Editions des archives contemporaines/IRD
* Mesurer la science, Vincent Larivière et Cassidy R. Sugimoto, 2018, Montréal PUM, coll. « Libre Accès »
* Handbook Bibliometrics, 2021, De Gruyter Saur



